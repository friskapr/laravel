<!--
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="/sapa" method="POST">
    @csrf
        <input type="text" name="nama">
        <input type="submit" value="masuk">
    </form>
</body>
</html>
-->

<!DOCTYPE html>

<html lang="en">

    <head>
      <title>Form</title>
    </head>

    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="GET">
    
        <label for="namadepan_user">First Name:</label><br><br>
        
            <input type="text" id="namadepan_user" name="fname">
        
        <br><br>
        <label for="namabelakang_user">Last Name:</label><br><br>
        
            <input type="text" id="namabelakang_user" name="lname">
        <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br>
        <br>
        <label>Nationality:</label><br><br>
        <select>
            <option value="indo">Indonesian</option>
            <option value="singa">Singaporean</option>
            <option value="malay">Malaysian</option>
            <option value="austr">Australian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa_user" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa_user" value="2">English<br>
        <input type="checkbox" name="bahasa_user" value="3">Other<br>
        <br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="sign up" >
        </form>
    
        
    </body>

</html>
